#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 29 13:33:51 2017

@author: louis
"""

class Playlist():
    """
    classe qui definit une playlist
    """

    def __init__(self):
        self.music_list = []
        self.name = ""

    def add_pist(self, pist):
        """
        add pist to the music list
        """

        self.music_list.append(pist)

    def set_name(self, name):
        """
        set the name of the playlist
        """

        self.name = name

    def get_total_time(self):
        """
        return the total time of the playlist
        """

        time = 0
        for pist in self.music_list:
            time += pist.duree

        return time
