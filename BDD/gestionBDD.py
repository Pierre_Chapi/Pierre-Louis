from configparser import ConfigParser
import psycopg2
import logging
from logging.handlers import RotatingFileHandler

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')
file_handler = RotatingFileHandler('messages.log', 'a', 1000000, 1)
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)
stream_handler = logging.StreamHandler()
stream_handler.setLevel(logging.INFO)
logger.addHandler(stream_handler)
 

def _config(filename='BDD/database.ini', section='postgresql'):
    # create a parser
    parser = ConfigParser()
    # read config file
    parser.read(filename)

    # get section, default to postgresql
    db = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            db[param[0]] = param[1]
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))
        logging.error("La section postgresql n'a pas trouvé le fichier database.ini")

    return db

def connect():
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        # read connection parameters
        params = _config()

        # connect to the PostgreSQL server
        logging.debug("Connecting to the PostgreSQL database:")
        conn = psycopg2.connect(**params)
        logging.debug("Database connection opened")

        return conn

    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)
        exit()

def disconnect(conn):
    conn.close()
    logging.debug("Database connection closed.")
