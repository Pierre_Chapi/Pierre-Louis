#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 27 14:48:14 2017

@author: louis
"""

import BDD.gestionBDD as gestionBDD
import Classes.Musique as Musique
import logging
    

def get_from_critere(item, args, temps):
    
    conn = gestionBDD.connect()
    cur = conn.cursor()    
    subtime = int(temps * int(args[1]) / 100)
    
    cur.execute("select * from morceaux where " + item + " ~ '" + args[0] + "' group by titre, album, artiste, genre, sousgenre, duree, format, polyphonie, chemin, random();")
    
    result = cur.fetchall()
    
    listmusique = []
    totaltime = 0
    for line in result:
        if totaltime + int(line[5]) <= subtime:
            totaltime += int(line[5])
            listmusique.append(Musique.Musique(line[0],line[1],line[2],line[3],line[4],line[5],line[6],line[7],line[8]))
            
    gestionBDD.disconnect(conn)
    logging.info("Durée totale en secondes de musiques pour cette requête : " + str(totaltime))
    return listmusique

def get_random(pourcent, temps):
    
    conn = gestionBDD.connect()
    cur = conn.cursor()    
    subtime = int(temps * int(pourcent) / 100)
    
    print(str(pourcent) + " total temps : " + str(temps))
    
    cur.execute("select * from morceaux group by titre, album, artiste, genre, sousgenre, duree, format, polyphonie, chemin, random();")
    
    result = cur.fetchall()
    
    listmusique = []
    totaltime = 0
    for line in result:
        if totaltime + int(line[5]) <= subtime:
            totaltime += int(line[5])
            listmusique.append(Musique.Musique(line[0],line[1],line[2],line[3],line[4],line[5],line[6],line[7],line[8]))
            
    print(str(totaltime))
    gestionBDD.disconnect(conn)
    return listmusique