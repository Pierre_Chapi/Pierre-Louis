#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    blablabla
"""

import argparse
from Classes import Playlist
import check_methodes
import logging


class Error(Exception):
    """
        blablabla
    """
    pass


class Pourcent_error(Error):
    """
        blablabla
    """


class myTuple(argparse.Action):
    """
        blablabla
    """
    total_percent = 0

    def __init__(self, option_strings, dest, nargs=None, **kwargs):
        self.dest = dest
        if nargs == 2:
            argparse.Action.__init__(self, option_strings,
                                     dest, nargs, **kwargs)

    def __call__(self, parser, namespace, values, option_strings):

        try:
            int(values[1])
            myTuple.total_percent += int(values[1])
            if myTuple.total_percent > 100:
                raise Pourcent_error
            if int(values[1]) <= 0:
                raise Pourcent_error
            values_list = []
            attr_dest = getattr(namespace, self.dest)
            if attr_dest:
                for item in attr_dest:
                    values_list.append(item)

            values_list.append(values)
            setattr(namespace, self.dest, values_list)
            setattr(namespace, "pourcent", myTuple.total_percent)
            # print("je suis dans le call : " + str(values[0]))
        except Pourcent_error:
            logging.error("Un pourcentage donné est supérieur à 100%")

            exit()
        except ValueError:
            logging.error("Error : --" + self.dest + " " + values[0] + " " + values[1])
            logging.error(self.dest + " argument values must be : str int")
            exit()


class CLI_manager():
    """
        blablabla
    """
    def __init__(self):
        parser = argparse.ArgumentParser()

        # creation d'un nouvel argument
        parser.add_argument('--genre', type=str, metavar='str int',
                            nargs=2, action=myTuple,
                            help="Kind of playlist and percentage of this kind")
        parser.add_argument('--artiste', type=str, metavar='str int',
                            nargs=2, action=myTuple,
                            help="artiste of playlist and percentage of this artiste")
        parser.add_argument('--duree', type=int, default=2000, metavar='int',
                            help="Duration of playlist")
        parser.add_argument('--debug', action='store_const', dest='loglevel', const=logging.DEBUG,
                            help=('set logging level to debug'))
        parser.add_argument('--info', action='store_const', dest='loglevel', const=logging.INFO,
                            help=('set logging level to info'))
        parser.add_argument('--warning', action='store_const', dest='loglevellogging.config.fileConfig()', const=logging.WARNING,
                            help=('set logging level to warning'))
        parser.add_argument('--error', action='store_const', dest='loglevel', const=logging.ERROR, 
                            help=('set logging level to error'))
        parser.add_argument('--critical', action='store_const', dest='loglevel', const=logging.CRITICAL,
                            help=('set logging level to critical'))
        #parser.add_argument('--xspf', action='store', dest='playlist.XSPF', type=str, default='',
                            #help=('Output as XSPF format in \the specified file'))
        #parser.add_argument('--m3u', action='store', dest='playlist.M3U', type=str, default='',
                            #help=('Output as M3U format in \ the specified file'))

        #parser.add_argument('name', type=str, help="Name of playlist")


        # recuperation des arguments passés en ligne de commande
        self.args = parser.parse_args()


# code a executer en fonction des arguments
def main():

    cli_manager = CLI_manager()
    play3 = Playlist.Playlist()

    print(str(cli_manager.args.pourcent))
    print(cli_manager.args.loglevel)
    print(cli_manager.args.__dict__)


    for item in cli_manager.args.__dict__:
        # print(str(item) + " : " + str(type(getattr(cli_manager.args, item))))
        # print(getattr(cli_manager.args, item))

        if isinstance(getattr(cli_manager.args, item), list):
            if getattr(cli_manager.args, item):
                logging.info("Requête pour : " + str(item))
                #print("requete pour : " + str(item))
                for attr in getattr(cli_manager.args, item):
                    for musi in check_methodes.get_from_critere(item, attr, cli_manager.args.duree):
                        play3.add_pist(musi)

    if cli_manager.args.pourcent < 100:
        logging.info("Requête aléatoire")
        for musi in check_methodes.get_random((100-cli_manager.args.pourcent), cli_manager.args.duree):
            play3.add_pist(musi)

    logging.info("Durée totale en secondes : " + str(play3.get_total_time()))
    #print("total : " + str(play3.get_total_time()))


    #if cli_manager.args.genre:
        #print(cli_manager.args.genre)
    #if cli_manager.args.artiste:
        #print(cli_manager.args.artiste)
    #if cli_manager.args.loglevel:
        #print(cli_manager.args.loglevel)



if __name__ == '__main__':
    main()
